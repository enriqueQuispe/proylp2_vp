/* Eventos según window */
	$(window).on({
		load: function(){
		},
		resize: function(){
		},
		scroll: function(){
		}
	})
/* js genereal */
	;(function($,undefined){
		"use strict";
		$(document).one('ready', iniciar);
	})(jQuery);
function iniciar(){
	/* call función comprobar navegador */
		comprobarnavegador();
	/* call para validar frm generales */
		validarfrmGeneral();
}
/* funnción para mostrar errores de validación */
	function showErrorCounter(e){
		if (e) {
			$('body').addClass('frmError');
			$("#contenedor-alerta").closest('body').addClass('showContenedor');
			$('input:submit').attr('disabled', 'disabled');
			setTimeout(function() {
				$('body').removeClass('showContenedor');
				$('input:submit').removeAttr('disabled');
			}, 3010);
		}
	}
/* Validar Formulario contáctanos */
	function validarfrmGeneral(){
		$('.frm').validate({
			showErrors: function(errorMap, errorList) {
				var counterError = this.numberOfInvalids();
				if (counterError == 1) {
					$("#contenedor-alerta .texto span").html("Se presentó " + counterError + " error.");
					this.defaultShowErrors();
				} else if(counterError>1) {
					$("#contenedor-alerta .texto span").html("Se presentaron " + counterError + " errores.");
					this.defaultShowErrors();
				}
			},
			highlight: function(element, errorClass) {
				$(element).addClass(errorClass);
			},
			unhighlight: function(element, errorClass, validClass){
				$(element).removeClass(errorClass).addClass(validClass);
			},
			invalidHandler: function(event, validator) {
				var existeError = validator.numberOfInvalids();
				showErrorCounter(existeError);
			},
			success: function(element) {
				element.closest('body').removeClass('frmError').addClass('frmSuccess');
			},
			submitHandler: function(form) {
				$('input:submit').removeAttr('disabled');
				console.log($(form).serialize());
				saveUserName();
				$('#suscripBlog')[0].reset();
				window.location.href = 'gracias.php';
			}
		});
	}
/* Función comprobar navegador */
	function comprobarnavegador() {
		console.log('Cargado comprobarnavegador')
		var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome/') > -1;
		var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox/') > -1;
		var is_safari = navigator.userAgent.toLowerCase().indexOf('safari/') > -1;
		var is_opera = navigator.userAgent.toLowerCase().indexOf('opera/') > -1;
		var is_ie = navigator.userAgent.toLowerCase().indexOf('msie') > -1;
		var isMobile = {
			Android: function() {
				return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
				return navigator.userAgent.match(/IEMobile/i);
			},
			any: function() {
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
			}
		};
		if (isMobile.any()) {
			/* si estamos en mobile */
		} else {
			switch (true) {
				case (is_chrome):
					break;
				case (is_firefox):
					break;
				case (is_safari):
					break;
				case (is_opera):
					break;
				default:
					body = document.getElementsByTagName('body')[0];
					var placeholder = document.createElement('script');
					placeholder.type = 'text/javascript';
					placeholder.async = true;
					placeholder.id = 'placeholder';
					placeholder.src = 'produccion/js/lib/utilitario/jquery.placeholder.js';
					body.appendChild(placeholder);
					break;
			}
		}
		/* saber si ie < ie9 */
			var oldIE;
			if ($('html').is('.ie6, .ie7, .ie8, .ie9')) {
				oldIE = true;
			}
			if (oldIE) {
				/* JS para IE old */
				window.location.href = 'http://browsehappy.com/?locale=es';
			} else {
				/* todo el cod normal */
			}
	}
