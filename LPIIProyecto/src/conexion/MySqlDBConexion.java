package conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class MySqlDBConexion {

	// Obtener las propiedades del archivo de recursos
	private static ResourceBundle rb =
					ResourceBundle.getBundle("conexion");
	
	static {
		try {
			//obtener el valor del archivo
			Class.forName(rb.getString("driver"));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Connection getConexion() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(
					rb.getString("url"), 
					rb.getString("username"),
					rb.getString("password"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return conn;
	}
}









