package controlador;

import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.UsuarioService;

import entidad.Usuario;

/**
 * Servlet implementation class ServletPelicula
 */
@WebServlet("/ServletSeguridad")
public class ServletSeguridad extends HttpServlet {
	private static final long serialVersionUID = 1L;

	UsuarioService service = new UsuarioService();

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String tipo = request.getParameter("tipo");

		// Se verifica que operacion
		if (tipo.toLowerCase().equals("usuario")) {
			// 1. Se obtienen los parametros propios del JSP registrar
			System.out.println("Obtener parámetros...");

			String usuario = request.getParameter("usuario");
			String clave = request.getParameter("clave");

			Usuario obj = new Usuario();
			obj.setUsuario(usuario);
			obj.setClave(clave);
			obj.setClave(clave);

			// 3. Se invoca al service
			obj = service.validar(obj);

			
			
			if (obj.getId() != 0) {
				// Se obtiene la session
				HttpSession session = request.getSession();

				System.out.println("Usuario encontrado");

				// se guarda en sesion el objeto que contiene los datos del
				// usuario
				session.setAttribute("usuario_sesion", obj);

				request.getRequestDispatcher("intranetprincipal.jsp").forward(
						request, response);
			} else {
				request.setAttribute("mensaje",
						"Verifique los datos ingresados.");
				request.getRequestDispatcher("/intranet.jsp").forward(request,
						response);
			}

		}
	}

}
