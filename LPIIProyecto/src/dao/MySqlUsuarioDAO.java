package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import conexion.MySqlDBConexion;
import entidad.Usuario;
import interfaces.UsuarioDAO;

public class MySqlUsuarioDAO implements UsuarioDAO {

	@Override
	public Usuario validar(Usuario obj) {
		Connection conn = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;

		try {
			// 1. Se obtiene la conexi�n
			conn = MySqlDBConexion.getConexion();

			// 2. Se define la sentencia (En este caso basta con un * FROM)
			String sql = "SELECT * FROM usuario WHERE usuario = ? AND clave = ?";

			// 3. Se obtiene un pstm
			pstm = conn.prepareStatement(sql);
			pstm.setString(1, obj.getUsuario());
			pstm.setString(2, obj.getClave());

			// 4. Se ejecuta y obtiene los resultados
			rs = pstm.executeQuery();

			// 5. Se itera los resultados
			while (rs.next()) {
				obj = new Usuario();

				obj.setId(rs.getInt("idusuario"));
				obj.setApellido(rs.getString("Apellidos"));
				obj.setNombre(rs.getString("nombre"));
				obj.setUsuario(rs.getString("usuario"));
				obj.setClave(rs.getString("clave"));
				obj.setCargo(rs.getString("cargo"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (pstm != null)
					pstm.close();
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return obj;
	}

}
