package interfaces;

import entidad.Usuario;

public interface UsuarioDAO {

	public Usuario validar(Usuario obj);
}
