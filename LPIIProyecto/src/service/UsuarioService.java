package service;

import interfaces.UsuarioDAO;
import entidad.Usuario;
import factory.Factory;

public class UsuarioService {

	Factory factory = Factory.getFactory(Factory.TIPO_MYSQL);
	UsuarioDAO dao = factory.getUsuarioDAO();

	public Usuario validar(Usuario obj) {
		return dao.validar(obj);
	}
}
