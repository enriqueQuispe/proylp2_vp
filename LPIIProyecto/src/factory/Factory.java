package factory;


import interfaces.UsuarioDAO;


public abstract class Factory {

	public static final int TIPO_MYSQL = 1;
	public static final int TIPO_SQLSERVER = 2;
	
	
	
	public abstract UsuarioDAO getUsuarioDAO();
	
	// Agregar la interface DAO
	
	public static Factory getFactory(int tipo){
		switch (tipo) {
		case TIPO_MYSQL:
			return new MySqlFactory();
		case TIPO_SQLSERVER:
			return null;
		default:
			return null;
		}
	}

}







